﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public bool paused;
    public GameObject menu;
    public void pause() {
        Debug.Log("Menu Scripte Girildi");
        paused = !paused;
        if(paused) {
            menu.SetActive(true);
        }
        if(!paused) {
            menu.SetActive(false);
        }
    }
}
