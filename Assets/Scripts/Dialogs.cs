﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class Dialogs : MonoBehaviour
{
    public Text dialog;
    public GameObject snowMan, TransitionObj , MainCamera;
    public int SMqueue , languageCode;
    public AudioSource SMVoiceSFX , HMVoiceSFX , SOBDDFY;
    public bool GameStart = false;

    void Update() {
        languageCode = PlayerPrefs.GetInt("language");
        if( Input.GetKeyDown(KeyCode.F) ) {
            SMqueue += 1;
            if(languageCode == 0) {
                SMDialog(SMqueue);
            }
        }
    }

    public void SMDialog(int SMqueue) {
        if(SMqueue == 1 || SMqueue == 1) {
            dialog.color = Color.white;
            dialog.text = "Hey sen nasıl girdin buraya";
            MainCamera.GetComponent<AnimationControl>().SMLookPlayer(true);
            MainCamera.GetComponent<AnimationControl>().SMAngryTalk(true);
            MainCamera.GetComponent<sounds>().SMDialogSound(1);
            StartCoroutine(reset(3f));
        }
        if(SMqueue == 2) {
            dialog.color = Color.white;
            dialog.text = "NÖBETÇİLER";
            MainCamera.GetComponent<AnimationControl>().SMAngryTalk(true);
            StartCoroutine(reset(0.5f));
            MainCamera.GetComponent<sounds>().SMDialogSound(2);
        }
        if(SMqueue == 3) {
            dialog.color = Color.white;
            dialog.text = "MELEKLER";
            MainCamera.GetComponent<AnimationControl>().SMAngryTalk(true);
            StartCoroutine(reset(0.5f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(3);
        }
        if(SMqueue == 4) {
            dialog.color = Color.white;
            dialog.text = "AAh, Gerci Onların Kanatları Anca Kendilerine Kadar";
            MainCamera.GetComponent<AnimationControl>().SMJeeringTalk(true);
            StartCoroutine(reset(3f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(4);
        }
        if(SMqueue == 5) {
            dialog.color = Color.white;
            dialog.text = "Pekala, Sanırım, Seni Kendim Dışarı Atmak Zorundayım";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(4f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(5);
        }
        if(SMqueue == 6) {
            dialog.color = Color.white;
            dialog.text = "Ama Çok Üşeniyorum";
            MainCamera.GetComponent<AnimationControl>().SMJeeringTalk(true);
            StartCoroutine(reset(2f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(6);
        }
        if(SMqueue == 7) {
            dialog.color = Color.black;
            dialog.text = "Sen de Kimsin?";
            // HMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(7);
        }
        if(SMqueue == 8) {
            dialog.color = Color.white;
            dialog.text = "Beni nasıl tanımazsın ben HBBKA";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(3f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(8);
        }
        if(SMqueue == 9) {
            dialog.color = Color.black;
            dialog.text = "HBBKA nedir?";
            // HMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(9);
        }
        if(SMqueue == 10) {
            dialog.color = Color.white;
            dialog.text = "Tabii ki de Her Boku Bilen Kardan Adam";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(3));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(10);
        }
        if(SMqueue == 11) {
            dialog.color = Color.black;
            dialog.text = "Burası neresi?";
            // HMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(11);
        }
        if(SMqueue == 12) {
            dialog.color = Color.white;
            dialog.text = "Burasıııı benim dünyadaki evim";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(2f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(12);
        }
        if(SMqueue == 13) {
            dialog.color = Color.white;
            dialog.text = "Çok gösterişli olduğu söylenemez";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(3f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(13);
        }
        if(SMqueue == 14) {
            dialog.color = Color.white;
            dialog.text = "Buraya arada sırada dizi izlemeye geliyorum. Genelde komedi ve dram oluyor";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(4f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(14);
        }
        if(SMqueue == 15) {
            dialog.color = Color.black;
            dialog.text = "Komedi ve dram mı?";
            // HMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(15);
        }
        if(SMqueue == 16) {
            dialog.color = Color.white;
            dialog.text = "Evet evet insanların yaptığı salak saçma şeyler işte";
            MainCamera.GetComponent<AnimationControl>().SMJeeringTalk(true);
            StartCoroutine(reset(4f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(16);
        }
        if(SMqueue == 17) {
            dialog.color = Color.white;
            dialog.text = "Bu haftaki bölüm çok heycanlı";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(2f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(17);
        }
        if(SMqueue == 18) {
            dialog.color = Color.white;
            dialog.text = "Dünyayı falan yok ettiler";
            MainCamera.GetComponent<AnimationControl>().SMTalk(true);
            StartCoroutine(reset(1f));
            // SMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(18);
        }
        if(SMqueue == 19) {
            dialog.color = Color.black;
            dialog.text = "Dünyayı falan mı yok ettiler?";
            // HMVoiceSFX.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(19);
        }
        if(SMqueue == 20) {
            dialog.color = Color.white;
            dialog.text = "ŞŞŞŞŞ, burda durdurma düğmesi falan yok";
            MainCamera.GetComponent<AnimationControl>().SMAngryTalk(true);
            StartCoroutine(reset(2f));
            // SOBDDFY.Play();
            MainCamera.GetComponent<sounds>().SMDialogSound(20);
        }
        if(SMqueue == 21) {
            MainCamera.GetComponent<AnimationControl>().SMSurprized(true);
            MainCamera.GetComponent<AnimationControl>().SMLookDown(true);
        }
        if(SMqueue == 22) {
            TransitionObj.GetComponent<transition>().GoTestChapter();
        }
    }
    IEnumerator reset(float Second) {
        yield return new WaitForSeconds(Second);
        resetFn();
    }
    public void resetFn() {
        MainCamera.GetComponent<AnimationControl>().SMAngryTalk(false);
        MainCamera.GetComponent<AnimationControl>().SMJeering(false);
        MainCamera.GetComponent<AnimationControl>().SMJeeringTalk(false);
        MainCamera.GetComponent<AnimationControl>().SMSmileTalking(false);
        MainCamera.GetComponent<AnimationControl>().SMTalk(false);
    }
}
