﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sounds : MonoBehaviour
{
    public AudioSource SMq1,
    SMq2,
    SMq3,
    SMq4,
    SMq5,
    SMq6,
    SMq7,
    SMq8,
    SMq9,
    SMq10,
    SMq11,
    SMq12,
    SMq13,
    SMq14,
    SMq15,
    SMq16,
    SMq17,
    SMq18,
    SMq19,
    SMq20;

    public void SMDialogSound(int queue) {
        if(queue == 1) {
            SMq1.Play();
        }
        if(queue == 2) {
            SoundMute(SMq1);
            SMq2.Play();
        }
        if(queue == 3) {
            SoundMute(SMq2);
            SMq3.Play();
        }
        if(queue == 4) {
            SoundMute(SMq3);
            SMq4.Play();
        }
        if(queue == 5) {
            SoundMute(SMq4);
            SMq5.Play();
        }
        if(queue == 6) {
            SoundMute(SMq5);
            SMq6.Play();
        }
        if(queue == 7) {
            SoundMute(SMq6);
            SMq7.Play();
        }
        if(queue == 8) {
            SoundMute(SMq7);
            SMq8.Play();
        }
        if(queue == 9) {
            SoundMute(SMq8);
            SMq9.Play();
        }
        if(queue == 10) {
            SoundMute(SMq9);
            SMq10.Play();
        }
        if(queue == 11) {
            SoundMute(SMq10);
            SMq11.Play();
        }
        if(queue == 12) {
            SoundMute(SMq11);
            SMq12.Play();
        }
        if(queue == 13) {
            SoundMute(SMq12);
            SMq13.Play();
        }
        if(queue == 14) {
            SoundMute(SMq13);
            SMq14.Play();
        }
        if(queue == 15) {
            SoundMute(SMq14);
            SMq15.Play();
        }
        if(queue == 16) {
            SoundMute(SMq15);
            SMq16.Play();
        }
        if(queue == 17) {
            SoundMute(SMq16);
            SMq17.Play();
        }
        if(queue == 18) {
            SoundMute(SMq17);
            SMq18.Play();
        }
        if(queue == 19) {
            SoundMute(SMq18);
            SMq19.Play();
        }
        if(queue == 20) {
            SoundMute(SMq19);
            SMq20.Play();
        }
    }
    public void SoundMute(AudioSource MuteSound) {
        MuteSound.Stop();
    }
}
