﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class TextLanguage : MonoBehaviour
{
    public Text startBtnText;
    // Start is called before the first frame update    
    void Update()
    {
        if(PlayerPrefs.GetInt("language") == 1) {
            startBtnText.text = "Start";
        }
        if(PlayerPrefs.GetInt("language") == 0) {
            startBtnText.text = "Başla";
        }
    }
}
