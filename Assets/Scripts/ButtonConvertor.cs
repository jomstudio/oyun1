﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ButtonConvertor : MonoBehaviour
{
    public GameObject MoveObj , AnimObj , MenusObj;

    public GameObject Player;
    public GameObject bulletRight;
    public GameObject bulletLeft;

    public void tusBaglayici(
    bool right , 
    bool left , 
    bool jump ,
    bool crouch ,
    bool climb ,
    bool RightFire ,
    bool LeftFire ,
    bool pause
    ) {
        Debug.Log("Convertor a geldi");
        if(left) {
            MoveObj.GetComponent<BasicActionScript>().MoveLeft();
        }
        if(right) {
            MoveObj.GetComponent<BasicActionScript>().MoveRight();
        }
        if(right && climb) {
            MoveObj.GetComponent<BasicActionScript>().MoveRight();
            MoveObj.GetComponent<BasicActionScript>().Climb(true);
        }
        if(left && climb) {
            MoveObj.GetComponent<BasicActionScript>().MoveLeft();
            MoveObj.GetComponent<BasicActionScript>().Climb(true);
        }
        if(jump) {
            MoveObj.GetComponent<BasicActionScript>().Jump();
        }
        if(crouch) {
            Player.GetComponent<Player>().egilTus();
        }
        if(climb) {
            MoveObj.GetComponent<BasicActionScript>().Climb(true);
        }
        if(RightFire) {
            bulletRight.GetComponent<RightBulletScript>().fire();
            MoveObj.GetComponent<BasicActionScript>().fire();
        }
        if(LeftFire) {
            bulletLeft.GetComponent<LeftBulletScript>().fire();
            MoveObj.GetComponent<BasicActionScript>().fire();
        }
        if(pause) {
            MenusObj.GetComponent<PauseMenu>().pause();
        }
        if(!climb) {
            MoveObj.GetComponent<BasicActionScript>().Climb(false);
        }
        if(!right && !left) {
            AnimObj.GetComponent<AnimationControl>().PLRunAnim(false);
        }
        if(!right && !left && !jump && !crouch && !climb && !RightFire && !LeftFire) {
            AnimObj.GetComponent<AnimationControl>().PLRunAnim(false);
            AnimObj.GetComponent<AnimationControl>().PLJumpAnim(false);
            AnimObj.GetComponent<AnimationControl>().PLComelAnim(false);
            Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0));
        }   
    }
}
