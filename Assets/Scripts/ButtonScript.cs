﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour , IPointerDownHandler , IPointerClickHandler , IPointerUpHandler { 

    public GameObject Player;
    public GameObject MainCamera;
    public GameObject Col;
    public GameObject PauseMenu;
    public bool jump , goLeft , goRight ;
    public bool crouch , climb , Paused;
    void Awake() {
        
    }

    void Update() {
       
    }

    void FixedUpdate() {
            if(Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("ClimbAnim")) {
                climb = true;
                if(goRight) {
                    MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                        true , 
                        false , 
                        false ,
                        false ,
                        false ,
                        false ,
                        false,
                    false
                    );
                }
                if(goLeft) {
                    MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                        false , 
                        true , 
                        false ,
                        false ,
                        false , 
                        false ,
                        false,
                    false
                    );
                }
            }
            if(!Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("ClimbAnim") ) {
                climb = false;
                if(goRight) {
                    MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                        true , 
                        false , 
                        false ,
                        false ,
                        false ,
                        false ,
                        false,
                    false
                    );
                }
                if(goLeft) {
                    MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                        false , 
                        true , 
                        false ,
                        false ,
                        false ,
                        false ,
                        false,
                    false
                    );
                }
            }
        }

    public void OnPointerDown(PointerEventData veri) {
        if(gameObject.name == "SolBtn") {
            goLeft = true;
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    true , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
        }
        if(gameObject.name == "SagBtn") {
            goRight = true;
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    true , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
        }
        if(gameObject.name == "FireBtn") {
            MainCamera.GetComponent<AnimationControl>().PLTakeAimAnim(true);

        }
        if(gameObject.name == "JumpBtn") {
            if(climb) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    true ,
                    false ,
                    false,
                    false
                );
            }   
        }
        if(gameObject.name == "SagBtn" && climb) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    true , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
        }
        if(gameObject.name == "SolBtn" && climb) {
            MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                true , 
                false ,
                false ,
                false ,
                false ,
                false,
                    false
            );
        }
    }
    public void OnPointerUp(PointerEventData veri) {
        if(gameObject.name == "SolBtn") {
            goLeft = false;
            if(Player.GetComponent<Animator>().GetBool("comel")) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                        false , 
                        false , 
                        false ,
                        true ,
                        false ,
                        false ,
                        false,
                    false
                    );
            }
            else {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
            }
            
        }
        if(gameObject.name == "FireBtn") {
            MainCamera.GetComponent<AnimationControl>().PLTakeAimAnim(false);
            MainCamera.GetComponent<AnimationControl>().PLFireAnim(true);
            bool PlayerRight = MainCamera.GetComponent<BasicActionScript>().lookRight;
            Debug.Log(PlayerRight);
            if(PlayerRight) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    true ,
                    false,
                    false
                );
            }
            if(!PlayerRight) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    true,
                    false
                );
            }
        }
        if(gameObject.name == "SagBtn") {
            goRight = false;
            if(Player.GetComponent<Animator>().GetBool("comel")) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    true ,
                    false ,
                    false ,
                    false,
                    false
                );
            }
            else {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
            }
        }
        if(gameObject.name == "JumpBtn" && climb) {
            MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                false , 
                false ,
                false ,
                false ,
                false ,
                false,
                    false
            );
        }
    }
    public void OnPointerClick(PointerEventData veri) {
        if(gameObject.name == "JumpBtn" && !climb) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                false , 
                true ,
                false ,
                false ,
                false ,
                false,
                    false
                );
            if(goRight) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                true , 
                false , 
                true ,
                false ,
                false ,
                false ,
                false,
                    false
                );
            }
            if(goLeft) {
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                true , 
                true ,
                false ,
                false ,
                false ,
                false,
                    false
                );
            } 
        }
        if(gameObject.name == "CrouchBtn") {
            crouch = !crouch;
            Debug.Log(crouch);
            if(crouch) {
                Col.GetComponent<Collider2D>().isTrigger = true;
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    true ,
                    false ,
                    false ,
                    false,
                    false
                );
            }
            if(!crouch) {
                Col.GetComponent<Collider2D>().isTrigger = false;
                MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
                MainCamera.GetComponent<AnimationControl>().PLComelAnim(false);
            }
        }
        if(gameObject.name == "PauseBtn") {
            PauseMenu.SetActive(true);
            Time.timeScale = 0f;
        }
        if(gameObject.name == "startBtn") {
            PauseMenu.SetActive(false);
            Time.timeScale = 1f;
        }
        if(gameObject.name == "CloseBtn") {
                MainCamera.GetComponent<transition>().close();
        }
    }
    void OnTriggerEnter2D(Collider2D veri) {
        if(veri.gameObject.tag == "Merdivenler") {
            climb  = true;
            MainCamera.GetComponent<AnimationControl>().PLTirmanAnim(true);
        }
        if(gameObject.tag == "egilmelik") {
            Debug.Log("Enter Çalıştı");
            MainCamera.GetComponent<DisButtonScript>().BtnDisable(
                true ,
                false ,
                false ,
                gameObject.name
            );
        }
    }
    void OnTriggerStay2D(Collider2D veri) {
        if(( gameObject.name == "egilmelik" ) && ( Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CrouchStayAnim") || Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("crouchWalk") ) ) {
            MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                false , 
                false ,
                true ,
                false ,
                false ,
                false,
                    false
            );
        }
            MainCamera.GetComponent<AnimationControl>().PLRunAnim(true);
        
    }
    void OnTriggerExit2D(Collider2D veri) {
        climb  = false;
        if(gameObject.name == "Merdivenler") {
            MainCamera.GetComponent<AnimationControl>().PLTirmanAnim(false);
            MainCamera.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
        }
        if(gameObject.name == "egilmelik") {
            MainCamera.GetComponent<DisButtonScript>().BtnDisable(
            false ,
            false ,
            false ,
            gameObject.name
        );
        }
    }
}

