﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class transition : MonoBehaviour
{
    public GameObject TimeLineCompo , MainCamera , DialogBox , SettingsBtn , StartBtn;
    public Canvas dialog;
    public Text Start , Settings;

    public void restartchapter() {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.buildIndex);
    }

    public void close() {
        Application.Quit();
    }

    public void GoTestChapter() {
        SceneManager.LoadScene(3);
    }

    public void goMenu() {
        SceneManager.LoadScene(1);
    }

    public void goSettings() {
        SceneManager.LoadScene(2);
    }

    public void goProlugue() {
        StartBtn.GetComponent<Animator>().SetTrigger("YokOl");
        SettingsBtn.GetComponent<Animator>().SetTrigger("YokOl");
        Settings.GetComponent<Animator>().SetTrigger("YokOl");
        Start.GetComponent<Animator>().SetTrigger("YokOl");
        StartCoroutine(TusKayb());
    }

    void OnTriggerEnter2D(Collider2D veri) {
        if(gameObject.name == "digersahneyegecis") {
            SceneManager.LoadScene(4);
        }
        if(veri.gameObject.tag == "Player") {
            restartchapter();
        }
    }

    IEnumerator TusKayb() {
        yield return new WaitForSeconds(1);
        TusKaybol();
    }
    public void TusKaybol() {
        Debug.Log("Prolugue'a Geçiyor");
        DialogBox.gameObject.SetActive(true);
        dialog.gameObject.SetActive(true);
        MainCamera.GetComponent<Dialogs>().SMDialog(1);
        MainCamera.GetComponent<Dialogs>().SMqueue += 1;
        MainCamera.GetComponent<DisButtonScript>().BtnDisable(
            false ,
            true ,
            true ,
            "Yok"
        );
    }
}
