﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public float MoveSpeed , health , DurationTime;
    public bool MoveRight , Moving;
    public GameObject Enemy;
    void Start()
    {
         Moving = true;
    }
    void Update() {
     if(MoveRight) {
          if(Moving) {
               transform.Translate(MoveSpeed , 0 , 0);
               transform.localScale = new Vector2(1 , 1);
          }
          if(!Moving) {
               transform.localScale = new Vector2(1 , 1);
          }
     }
       if(!MoveRight) {
          if(Moving) {
               transform.Translate(-MoveSpeed , 0 , 0);
               transform.localScale = new Vector2(-1 , 1);
          }
          if(!Moving) {
               transform.localScale = new Vector2(-1 , 1);
          }
       }
    }
    void OnTriggerEnter2D (Collider2D veri) {
     if(veri.gameObject.tag == "bullet") {
          health -= 1; 
     }
     
     if(veri.gameObject.tag == "turn") {
          StartCoroutine(Duration(DurationTime));
          Moving = false;
          Enemy.GetComponent<Animator>().SetBool("stay" , true);
     }
    }
    IEnumerator Duration(float DurationTime) {
         yield return new WaitForSeconds(DurationTime);
         Durati();
    }
    public void Durati() {
         MoveRight = !MoveRight;
         Moving = true;
         Enemy.GetComponent<Animator>().SetBool("stay" , false);
    }
}
