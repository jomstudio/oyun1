﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class KeyboardScript : MonoBehaviour
{
    public GameObject ButtonObj , Player , MoveObj , AnimObj;
    public bool jump , goLeft , goRight , crouch , climb , Paused;
    void Update() {


        if(!Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("ClimbAnim") ) {
                climb = false;
                if(goRight) {
                    ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                        true , 
                        false , 
                        false ,
                        false ,
                        false ,
                        false ,
                        false,
                    false
                    );
                }
                if(goLeft) {
                    ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                        false , 
                        true , 
                        false ,
                        false ,
                        false ,
                        false ,
                        false,
                    false
                    );
                }
            }

            if(Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("ClimbAnim")) {
                climb = true;
                if(goRight) {
                    ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                        true , 
                        false , 
                        false ,
                        false ,
                        false ,
                        false ,
                        false,
                    false
                    );
                }
                if(goLeft) {
                    ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                        false , 
                        true , 
                        false ,
                        false ,
                        false , 
                        false ,
                        false,
                    false
                    );
                }
            }

        if( Input.GetKeyDown(KeyCode.A) ) {
            goLeft = true;
            ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                true , 
                false ,
                false ,
                false ,
                false ,
                false,
                    false
            );
        }

        if( Input.GetKeyUp(KeyCode.A) ) {
            goLeft = false;
            if(Player.GetComponent<Animator>().GetBool("comel")) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                        false , 
                        false , 
                        false ,
                        true ,
                        false ,
                        false ,
                        false,
                    false
                    );
            }
            else {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
            }
        }

        if( Input.GetKeyDown(KeyCode.D) ) {
            goRight = true;
            ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                true , 
                false , 
                false ,
                false ,
                false ,
                false ,
                false,
                    false
            );
        }

        if( Input.GetKeyUp(KeyCode.D) ) {
            goRight = false;
            if(Player.GetComponent<Animator>().GetBool("comel")) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    true ,
                    false ,
                    false ,
                    false,
                    false
                );
            }
            else {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
            }
        }

        if( Input.GetKeyDown(KeyCode.W) ) {
            if(climb) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    true ,
                    false ,
                    false,
                    false
                );
            }
            if(!climb) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                false , 
                true ,
                false ,
                false ,
                false ,
                false,
                    false
                );
            if(goRight) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                true , 
                false , 
                true ,
                false ,
                false ,
                false ,
                false,
                    false
                );
            }
            if(goLeft) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                true , 
                true ,
                false ,
                false ,
                false ,
                false,
                    false
                );
            }
        }
        }

        if( Input.GetKeyUp(KeyCode.W) ) { 
            ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                false , 
                false ,
                false ,
                false ,
                false ,
                false,
                    false
            );
        }

        if( Input.GetKeyDown(KeyCode.A) && Input.GetKeyDown(KeyCode.W) ) {
            ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                true , 
                false ,
                false ,
                false ,
                false ,
                false,
                    false
            );
        }

        if( Input.GetKeyDown(KeyCode.D) && Input.GetKeyDown(KeyCode.W) ) {
            ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    true , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
        }

        if( Input.GetKeyDown(KeyCode.T) ) {
            AnimObj.GetComponent<AnimationControl>().PLTakeAimAnim(true);
        }

        if( Input.GetKeyUp(KeyCode.T) ) {
            AnimObj.GetComponent<AnimationControl>().PLTakeAimAnim(false);
            AnimObj.GetComponent<AnimationControl>().PLFireAnim(true);
            bool PlayerRight = MoveObj.GetComponent<BasicActionScript>().lookRight;
            Debug.Log(PlayerRight);
            if(PlayerRight) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    true ,
                    false,
                    false
                );
            }
            if(!PlayerRight) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    true,
                    false
                );
            }

        if( Input.GetKeyDown(KeyCode.W) && climb ) {
            ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                false , 
                false , 
                false ,
                false ,
                false ,
                false ,
                false,
                    false
            );
        }

    }

    if( Input.GetKeyDown(KeyCode.C) ) {
            crouch = !crouch;
            Debug.Log(crouch);
            if(crouch) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    true ,
                    false ,
                    false ,
                    false ,
                    false
                );
            }
            if(!crouch) {
                ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false,
                    false
                );
                AnimObj.GetComponent<AnimationControl>().PLComelAnim(false);
            }
        }
        if( Input.GetKeyDown(KeyCode.Escape) ) {
            ButtonObj.GetComponent<ButtonConvertor>().tusBaglayici(
                    false , 
                    false , 
                    false ,
                    false ,
                    false ,
                    false ,
                    false ,
                    true
                );
        }
    }
}