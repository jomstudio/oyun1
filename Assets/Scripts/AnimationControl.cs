﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControl : MonoBehaviour
{
    public GameObject Player;
    public GameObject SnowMan;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PLTirmanAnim(bool climb) {
        if(climb) {
            Player.GetComponent<Animator>().SetBool("climb" , true);
        }
        if(!climb) {
            Player.GetComponent<Animator>().SetBool("climb" , false);
        }
    }

    public void PLComelAnim(bool crouch) {
        if(crouch) {
            Player.GetComponent<Animator>().SetBool("crouch" , true);
        }
        if(!crouch) {
            Player.GetComponent<Animator>().SetBool("crouch" , false);
        }
    }

    public void PLRunAnim(bool run) {
        if(run) {
            Player.GetComponent<Animator>().SetBool("run" , true);
        }
        if(!run) {
            Player.GetComponent<Animator>().SetBool("run" , false);
        }
    }

    public void PLFallAnim(bool fall) {
        if(fall) {
            Player.GetComponent<Animator>().SetBool("fall" , true);
        }
        if(!fall) {
            Player.GetComponent<Animator>().SetBool("fall" , false);
        }
    }

    public void PLJumpAnim(bool jump) {
        if(jump) {
            Player.GetComponent<Animator>().SetTrigger("jump");
        }
        if(!jump) {
            Player.GetComponent<Animator>().ResetTrigger("jump");
        }
    }

    public void PLFireAnim(bool fire) {
        if(fire) {
            Player.GetComponent<Animator>().SetBool("fire" , true);
        }
        if(!fire) {
            Player.GetComponent<Animator>().SetBool("fire" , false);
        }
    }

    public void PLTakeAimAnim(bool TakeAim) {
        if(TakeAim) {
            Player.GetComponent<Animator>().SetBool("TakeAim" , true);
        }
        if(!TakeAim) {
            Player.GetComponent<Animator>().SetBool("TakeAim" , false);
        }
    }

    public void SMJeering(bool jeering) {
        if(jeering) {
            SnowMan.GetComponent<Animator>().SetBool("Jeering" , true);
        }
        if(!jeering) {
            SnowMan.GetComponent<Animator>().SetBool("Jeering" , false);
        }
    }

    public void SMJeeringTalk(bool jeeringTalk) {
        if(jeeringTalk) {
            SnowMan.GetComponent<Animator>().SetBool("JeeringTalk" , true);
        }
        if(!jeeringTalk) {
            SnowMan.GetComponent<Animator>().SetBool("JeeringTalk" , false);
        }
    }

    public void SMSurprized(bool surprized) {
        if(surprized) {
            SnowMan.GetComponent<Animator>().SetBool("Surprized" , true);
        }
        if(!surprized) {
            SnowMan.GetComponent<Animator>().SetBool("Surprized" , false);
        }
    }

    public void SMSmileTalking(bool SmileTalk) {
        if(SmileTalk) {
            SnowMan.GetComponent<Animator>().SetBool("SmileTalking" , true);
        }
        if(!SmileTalk) {
            SnowMan.GetComponent<Animator>().SetBool("SmileTalking" , false);
        }
    }

    public void SMLookPlayer(bool LookPlayer) {
        if(LookPlayer) {
            SnowMan.GetComponent<Animator>().SetBool("LookPlayer" , true);
        }
        if(!LookPlayer) {
            SnowMan.GetComponent<Animator>().SetBool("LookPlayer" , false);
        }
    }

    public void SMLookDown(bool LookDown) {
        if(LookDown) {
            SnowMan.GetComponent<Animator>().SetBool("LookDown" , true);
        }
        if(!LookDown) {
            SnowMan.GetComponent<Animator>().SetBool("LookDown" , false);
        }
    }

    public void SMAngryTalk(bool AngryTalk) {
        if(AngryTalk) {
            SnowMan.GetComponent<Animator>().SetBool("AngryTalk" , true);
        }
        if(!AngryTalk) {
            SnowMan.GetComponent<Animator>().SetBool("AngryTalk" , false);
        }
    }
    public void SMTalk(bool Talk) {
        if(Talk) {
            SnowMan.GetComponent<Animator>().SetBool("Talk" , true);
        }
        if(!Talk) {
            SnowMan.GetComponent<Animator>().SetBool("Talk" , false);
        }
    }
}
