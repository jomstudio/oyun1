﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicActionScript : MonoBehaviour
{
    public bool lookRight, OntheGround , jump , climb;
    public float MoveSpeed , jumpForce , caliber , climbSpeed , maxVelocity;
    public GameObject Player , AnimObj , bulletLeft , bulletRight;
    public Transform[] PlayerPointofContact;
    public LayerMask whichFloor;
    public Vector2 bulletPos;
    void Start() {
        lookRight = true;
    }
    void Update() {
        if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("StayAnim")) {
            AnimObj.GetComponent<AnimationControl>().PLFireAnim(false);
        }
        if(climb) {
            Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, climbSpeed));
        }
        if(!OntheGround) {
            Player.GetComponent<Transform>().Translate(0,0,0);
        }
        if(OntheGround) {
            MoveSpeed = 10;
        }
    }
    void FixedUpdate() {
        OntheGround = ontheGround();
        if(Player.GetComponent<Rigidbody2D>().velocity.y < 0) {
            AnimObj.GetComponent<AnimationControl>().PLFallAnim(true);
        }
    }
    public void MoveLeft() {
        AnimObj.GetComponent<AnimationControl>().PLRunAnim(true);
        if(lookRight) {
            lookRight = !lookRight;
            Vector3 yon = Player.GetComponent<Transform>().transform.localScale;
            yon.x *= -1f;
            Player.GetComponent<Transform>().localScale = yon;
        }
        float XDurdur = 0f;
        float surat = Mathf.Abs(Player.GetComponent<Rigidbody2D>().velocity.x);
        if(surat < maxVelocity) {
            XDurdur = -MoveSpeed;
        }
        Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(XDurdur , 0));
    }
    public void MoveRight() {
        AnimObj.GetComponent<AnimationControl>().PLRunAnim(true);
            if(!lookRight) {
                lookRight = !lookRight;
                Vector3 yon = Player.GetComponent<Transform>().transform.localScale;
                yon.x *= -1f;
                Player.GetComponent<Transform>().localScale = yon;
            }
        float XDurdur = 0f;
        float surat = Mathf.Abs(Player.GetComponent<Rigidbody2D>().velocity.x);
        if(surat < maxVelocity) {
            XDurdur = -MoveSpeed;
        }
        Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(-XDurdur , 0));
        }
    public void Jump() {
        if(OntheGround) {
            Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            AnimObj.GetComponent<AnimationControl>().PLJumpAnim(true);
            OntheGround = false;
            jump = true;
            AnimObj.GetComponent<AnimationControl>().PLFallAnim(false);
        }

    }
    public void fire() {
        float bulletX = Player.GetComponent<Transform>().transform.position.x;
        float bulletY = Player.GetComponent<Transform>().transform.position.y + 0.35f;
        bulletPos = new Vector2(bulletX , bulletY);
        if(lookRight) {
            Instantiate(bulletRight , bulletPos , Quaternion.identity);
        }
        if(!lookRight) {
            Instantiate(bulletLeft , bulletPos , Quaternion.identity);
        } 
    }
    public void Climb( bool climbBtn ){
        if(climbBtn) {
            climb = true;
        }
        if(!climbBtn) {
            climb = false;
        }   
    }
    private bool ontheGround() {
        if (Player.GetComponent<Rigidbody2D>().velocity.y <= 0) {
            foreach (Transform nokta in PlayerPointofContact)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(nokta.position, caliber, whichFloor);

                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        AnimObj.GetComponent<AnimationControl>().PLFallAnim(false);
                        AnimObj.GetComponent<AnimationControl>().PLJumpAnim(false);
                        jump = false;
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
