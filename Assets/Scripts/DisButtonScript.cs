﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class DisButtonScript : MonoBehaviour {
    public Button CrouchButton;
    public Button JumpButton;
    public Button StartButton , SettingsButton;

    public void BtnDisable(
        bool crouch ,
        bool StartBtn ,
        bool SettingsBtn ,
        string colliderİsim
    ) {
        if(crouch && colliderİsim == "egilmelik") {
            CrouchButton.interactable = false;
            JumpButton.interactable = false;
            CrouchButton.gameObject.SetActive(false);
            JumpButton.gameObject.SetActive(false);
        }
        if(!crouch && colliderİsim == "egilmelik") {
            CrouchButton.gameObject.SetActive(true);
            JumpButton.gameObject.SetActive(true);
            CrouchButton.interactable = true;
            JumpButton.interactable = true;
        }
        if(StartBtn) {
            StartButton.interactable = false;
            StartButton.gameObject.SetActive(false);
        }
        if(SettingsBtn) {
            SettingsButton.gameObject.SetActive(false);
            SettingsButton.interactable = false;
        }
    }
}
