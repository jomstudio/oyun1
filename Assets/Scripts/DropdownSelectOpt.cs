﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class DropdownSelectOpt : MonoBehaviour
{
    public Dropdown languageDD;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("language") == 0) {
            languageDD.value = 0;
        }
        if(PlayerPrefs.GetInt("language") == 1) {
            languageDD.value = 1;
        }
    }

    
}
