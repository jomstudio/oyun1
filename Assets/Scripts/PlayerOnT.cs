using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerOnT : MonoBehaviour 
{
    public GameObject MainCamera;
    void OnTriggerEnter2D (Collider2D veri) {
        if(veri.gameObject.tag == "Merdivenler") {
            MainCamera.GetComponent<AnimationControl>().PLTirmanAnim(true);
        }
    }
    void OnTriggerExit2D (Collider2D veri) {
        if(veri.gameObject.tag == "Merdivenler") {
            MainCamera.GetComponent<AnimationControl>().PLTirmanAnim(false);
        }
    }
}